+++
title = "Hai"
author = ["Hilal Hadyanto"]
date = 2021-12-08T20:59:00+08:00
tags = ["emacs", "setup", "spacemacs"]
draft = false
+++

Mencoba membuat blog dengan menggunakan [Hugo](https://gohugo.io/) + [Emacs](https://www.gnu.org/software/emacs/) ([spacemacs](https://www.spacemacs.org/)) + [ox-hugo](https://ox-hugo.scripter.co/)